import React, { Component } from 'react'
import AWS from 'aws-sdk';
import { SignalingClient, Role } from 'amazon-kinesis-video-streams-webrtc';
import { v4 as uuidv4 } from 'uuid';

interface SimpleKVSViewerProps {
    streamName: string,
    region: string,
    awsConfig: AWS.Config,
}
interface SimpleKVSViewerState { }

class SimpleKVSViewer extends Component<SimpleKVSViewerProps, SimpleKVSViewerState> {
    videoRef: any;
    viewer: any = {
        remoteView: null,
        remoteStream: null,
        signalingClient: null,
        peerConnection: null,
    };
    
    constructor(props: any) {
        super(props);
        this.videoRef = React.createRef();
        this.startPlayback = this.startPlayback.bind(this);
        this.stopViewer = this.stopViewer.bind(this);
    }
    
    async componentDidMount() {
        await this.startPlayback('');
    }

    private kinesisVideoClient(connectionInfo: any): AWS.KinesisVideo {
        // Create KVS client
        return new AWS.KinesisVideo({
            region: connectionInfo.region,
            accessKeyId: connectionInfo.accessKeyId,
            secretAccessKey: connectionInfo.secretAccessKey,
            sessionToken: connectionInfo.sessionToken,
            endpoint: connectionInfo.endpoint,
            correctClockSkew: true,
        });
    }
    
    private kinesisSignalingClient(connectionInfo: any, httpsEndpoint: string) {
        return new AWS.KinesisVideoSignalingChannels({
            region: connectionInfo.region,
            accessKeyId: connectionInfo.accessKeyId,
            secretAccessKey: connectionInfo.secretAccessKey,
            sessionToken: connectionInfo.sessionToken,
            endpoint: httpsEndpoint,
            correctClockSkew: true
        });
    }
    
    private async channelARN(kinesisVideoClient: any, channelName: string) {
        const describeSignalingChannelResponse = await kinesisVideoClient.describeSignalingChannel({
            ChannelName: channelName,
        })
        .promise();
        
        if (!describeSignalingChannelResponse.ChannelInfo) {
            return
        }
        const channelARN = describeSignalingChannelResponse.ChannelInfo.ChannelARN;
        return channelARN;
    }
    
    private async signalingEndPoints(kinesisVideoClient: any, channelARN: string) {
        // Get signaling channel endpoints
        const getSignalingChannelEndpointResponse = await kinesisVideoClient.getSignalingChannelEndpoint({
                ChannelARN: channelARN,
                SingleMasterChannelEndpointConfiguration: {
                    Protocols: ['WSS', 'HTTPS'],
                    Role: Role.VIEWER,
                },
            })
            .promise();
                
        if (!getSignalingChannelEndpointResponse) {
          console.log('Channel Endpoint Response is null');
          return
        }
        
        const endpointList = getSignalingChannelEndpointResponse.ResourceEndpointList;
        if (!endpointList) {
          console.log('Channel Endpoint List');
          return
        }
    
        var endpointsByProtocol: any = {};
        
        endpointList.forEach((endpoint: any) => {
            if (endpoint.Protocol) {
                endpointsByProtocol[endpoint.Protocol] = endpoint.ResourceEndpoint;
            }
        })
        
        return endpointsByProtocol;
    }
    
    private async iceServers(kinesisVideoSignalingChannelsClient: any, channelARN: string, region: string, natTraversalDisabled: boolean, forceTURN: boolean) {
            
        // Get ICE server configuration
        const getIceServerConfigResponse = await kinesisVideoSignalingChannelsClient
            .getIceServerConfig({
                ChannelARN: channelARN,
            })
            .promise();
        const iceServers = [];
        if (!natTraversalDisabled && !forceTURN) {
            iceServers.push({ urls: `stun:stun.kinesisvideo.${region}.amazonaws.com:443` });
        }
        if (natTraversalDisabled) {
            var iceServerList = getIceServerConfigResponse.IceServerList;
            
            if (!iceServerList) {
                console.log('[VIEWER] No ICE servers: ');
                return;
            }
            
            iceServerList.forEach((iceServer: any) =>
                iceServers.push({
                    urls: iceServer.Uris,
                    username: iceServer.Username,
                    credential: iceServer.Password,
                }),
            );
        }
        return iceServers;
    }
    
    async startViewer(remoteView: HTMLVideoElement, formValues: any) {
        this.viewer.remoteView = remoteView
        
        const kinesisVideoClient = this.kinesisVideoClient(formValues);
        const channelARN = await this.channelARN(kinesisVideoClient, formValues.channelName);
        if (!channelARN) {
            console.log('Unable to determine channel ARN')
            return
        }
        
        const endpointsByProtocol = await this.signalingEndPoints(kinesisVideoClient, channelARN);
        if (!endpointsByProtocol) {
          console.log('Unable to determine endpoints')
          return
        }
        
        const kinesisVideoSignalingChannelsClient = this.kinesisSignalingClient(formValues, endpointsByProtocol.HTTPS);
        const iceServers = await this.iceServers(kinesisVideoSignalingChannelsClient, channelARN, formValues.region, formValues.natTraversalDisabled, formValues.forceTURN);
        
        if (!iceServers) {
            console.log('Null ICE servers: ')
            return
        }
    
        // Create Signaling Client
        this.viewer.signalingClient = new SignalingClient({
            channelARN,
            channelEndpoint: endpointsByProtocol.WSS,
            clientId: formValues.clientId,
            role: Role.VIEWER,
            region: formValues.region,
            credentials: {
                accessKeyId: formValues.accessKeyId,
                secretAccessKey: formValues.secretAccessKey,
                sessionToken: formValues.sessionToken,
            },
            systemClockOffset: kinesisVideoClient.config.systemClockOffset,
        });
    
        const configuration: RTCConfiguration = {
            iceServers,
            iceTransportPolicy: formValues.forceTURN ? 'relay' : 'all',
        };
        
        this.viewer.peerConnection = new RTCPeerConnection(configuration);
        
        this.viewer.signalingClient.on('open', async () => {
            console.log('Connected to signaling service');
    
            // Create an SDP offer to send to the master
            console.log('Creating SDP offer');
            await this.viewer.peerConnection.setLocalDescription(
                await this.viewer.peerConnection.createOffer({
                    offerToReceiveAudio: false,
                    offerToReceiveVideo: true,
                }),
            );
    
            // When trickle ICE is enabled, send the offer now and then send ICE candidates as they are generated. Otherwise wait on the ICE candidates.
            if (formValues.useTrickleICE) {
                console.log('Sending SDP offer');
                this.viewer.signalingClient.sendSdpOffer(this.viewer.peerConnection.localDescription);
            }
            console.log('Generating ICE candidates');
        });
    
        this.viewer.signalingClient.on('sdpAnswer', async (answer: any) => {
            // Add the SDP answer to the peer connection
            console.log('Received SDP answer');
            await this.viewer.peerConnection.setRemoteDescription(answer);
        });
    
        this.viewer.signalingClient.on('iceCandidate', (candidate: any) => {
            // Add the ICE candidate received from the MASTER to the peer connection
            console.log('Received ICE candidate');
            this.viewer.peerConnection.addIceCandidate(candidate);
        });
    
        this.viewer.signalingClient.on('close', () => {
            console.log('Disconnected from signaling channel');
            this.stopViewer();
        });
    
        this.viewer.signalingClient.on('error', (error: any) => {
            console.error('Signaling client error: ', error);
        });
    
        // Send any ICE candidates to the other peer
        this.viewer.peerConnection.addEventListener('icecandidate', ({ candidate }: any) => {
            if (candidate) {
                console.log('Generated ICE candidate', candidate);
    
                // When trickle ICE is enabled, send the ICE candidates as they are generated.
                if (formValues.useTrickleICE) {
                    console.log('Sending ICE candidate');
                    this.viewer.signalingClient.sendIceCandidate(candidate);
                }
            } else {
                console.log('All ICE candidates have been generated');
    
                // When trickle ICE is disabled, send the offer now that all the ICE candidates have ben generated.
                if (!formValues.useTrickleICE) {
                    console.log('Sending SDP offer');
                    this.viewer.signalingClient.sendSdpOffer(this.viewer.peerConnection.localDescription);
                }
            }
        });
    
        // As remote tracks are received, add them to the remote view
        this.viewer.peerConnection.addEventListener('track', (event: any) => {
            console.log('Received remote track');
            if (remoteView.srcObject) {
                return;
            }
            this.viewer.remoteStream = event.streams[0];
            remoteView.srcObject = event.streams[0];
            remoteView.play();
        });
    
        console.log('Starting viewer connection');
        this.viewer.signalingClient.open();
    }
    
    
    stopViewer() {
        if (this.viewer.remoteView) {
            this.viewer.remoteView.srcObject = null;
            this.viewer.remoteView.pause();
        }
        
        if (this.viewer.remoteStream) {
            console.log('Stopping remoteStream');
            this.viewer.remoteStream.getTracks().forEach((track: any) => track.stop());
            this.viewer.remoteStream = null;
        }
        
        if (this.viewer.peerConnection) {
            console.log('Stopping peerConnection', this.viewer.peerConnection);
            this.viewer.peerConnection.close();
            this.viewer.peerConnection = null;
        }
        
        if (this.viewer.signalingClient) {
            console.log('Stopping signalingClient');
            this.viewer.signalingClient.close();
            this.viewer.signalingClient = null;
        }
    }

    async startPlayback(event: any) {
        if (!this.props.awsConfig.credentials) {
            throw new Error ("Null credentials")
        }

        const form = {
            region: this.props.region,
            accessKeyId: this.props.awsConfig.credentials.accessKeyId,
            secretAccessKey: this.props.awsConfig.credentials.secretAccessKey,
            sessionToken: this.props.awsConfig.credentials.sessionToken,
            endpoint: null,
            clientId: uuidv4(),
            correctClockSkew: true,
            channelName: this.props.streamName,
            useTrickleICE: true,
        }
        this.startViewer(this.videoRef.current, form);
    }
    
    render() {
        const videoStyle = {
            backgroundColor: "black",
            lineHeight: 0,
            display: "inline-block",
            width: "100%",
            maxWidth: "100%"
        };
        
        return (
            <div>
                <video style={videoStyle} ref={this.videoRef} controls/>
            </div>
        );
    }
    
}

export default SimpleKVSViewer;

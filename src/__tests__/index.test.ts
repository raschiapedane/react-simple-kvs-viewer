import SimpleKVSViewer from '../index'
import AWSConfig from './aws-exports'
import AWSSession from './AWSSession'
import AWS from 'aws-sdk';
import { v4 as uuidv4 } from 'uuid';


var a: any = undefined
var formValues: any = undefined
var kinesisVideoClient: any = undefined
var channelARN: any = undefined
var endpoints: any = undefined

beforeAll(async () => {
    var awsSession = new AWSSession();
    expect(await awsSession.authorize(AWSConfig.user, AWSConfig.password)).toBeTruthy();
    expect(await awsSession.isSessionValid()).toBeTruthy();

    var token = await awsSession.sessionJWTToken();
    expect(token).toBeDefined();
    awsSession.identityCredentials(token);

    var credential = await (AWS.config.credentials as any).getPromise()
    expect(AWS.config.credentials).toBeDefined()

    a = new SimpleKVSViewer({ streamName: AWSConfig.streamName, region: AWSConfig.aws_cognito_region, awsConfig: AWS.config })
    expect(a).toBeDefined()
    
    if (AWS.config.credentials) {
        formValues = {
            region: AWSConfig.aws_cognito_region,
            accessKeyId: AWS.config.credentials.accessKeyId,
            secretAccessKey: AWS.config.credentials.secretAccessKey,
            sessionToken: AWS.config.credentials.sessionToken,
            endpoint: null,
            clientId: uuidv4(),
            correctClockSkew: true,
            channelName: AWSConfig.streamName,
            useTrickleICE: true,
        }
    }

    kinesisVideoClient = a.kinesisVideoClient(formValues);
    expect(kinesisVideoClient).toBeDefined();
})

test('Test Instance', async () => {
    expect(a).toBeDefined()
});

test('Test Channel ARN', async () => {
    channelARN = await a.channelARN(kinesisVideoClient, formValues.channelName);
    expect(channelARN).toBeDefined();
});

test('Test Endpoints', async () => {
    endpoints = await a.signalingEndPoints(kinesisVideoClient, channelARN);
    expect(endpoints).toHaveProperty('HTTPS')
    expect(endpoints).toHaveProperty('WSS')
});

test('Test ICE', async () => {
    const kinesisVideoSignalingChannelsClient = a.kinesisSignalingClient(formValues, endpoints.HTTPS);
    const iceServers = await a.iceServers(kinesisVideoSignalingChannelsClient, channelARN, formValues.region, formValues.natTraversalDisabled, formValues.forceTURN);
    expect(iceServers).toBeDefined()
    expect(iceServers.length).toBeGreaterThan(0)
});




const AWSEndPoints = {
    "aws_cognito_region": "COGNITO_REGION",
    CognitoData: {
        UserPoolId: "COGNITO_USER_POOL_ID",
        ClientId: "COGNITO_APP_ID",
        IndentityPoolId: "COGNITO_IDENTITY_POOL_ID"
    },
    ApiGw: "OPTIONAL_API_GW_ENDPOINT",
    WSGw: "OPTIONAL_WSG_ENDPOINT",
    user: "MY_USER",
    password: "MY_PWD!",
    streamName: "MY_KVS_STREAM"
};

export default AWSEndPoints;

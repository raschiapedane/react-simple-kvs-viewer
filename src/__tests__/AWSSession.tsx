import AWSConfig from './aws-exports'
import AWS from 'aws-sdk'
import * as AmazonCognitoIdentity from 'amazon-cognito-identity-js'
import Axios, { Method } from 'axios'

class AWSSession {
    userPool = new AmazonCognitoIdentity.CognitoUserPool(AWSConfig.CognitoData)
    user = this.userPool.getCurrentUser()
    
    identityCredentials(JwtToken: string) {
        if (AWSConfig.CognitoData.IndentityPoolId) {

    		AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    			IdentityPoolId: AWSConfig.CognitoData.IndentityPoolId,
    			Logins: {
    			    [`cognito-idp.${AWSConfig.aws_cognito_region}.amazonaws.com/${AWSConfig.CognitoData.UserPoolId}`]: JwtToken
    			}
    		}, {region: AWSConfig.aws_cognito_region});
        }
    }

    async sessionJWTToken() {
        try {
            const session:any = await this.session()
            if (session) {
                const JwtToken = session.getIdToken().getJwtToken()
                return JwtToken
            }
            return null
        } catch (e) {
            console.log('Error retrieving session')
            return null
        }
    }

    async callApi(method: Method, api: string, headers: any, data: any) {
        const token = await this.sessionJWTToken()

        if (token) {
            headers["X-Amz-Security-Token"] = token
        }

        let config = {
            method: method,
            headers: headers,
            data: data
        }
        
        //redirect Axios errors.
        Axios.interceptors.response.use(response => {
            return response
        }, error => {
            throw (error)
        }); 
        
        try {
            return await Axios(AWSConfig.ApiGw + api, config)
        } catch (error) {
            throw error
        }
    }

    session() {
        if (!this.user) {
            this.user = this.userPool.getCurrentUser()
        }

        if (this.user !== null) {
            return new Promise ((resolve, reject) => {
                this.user!.getSession((err: any, session: any) => {
                    if (err) {
                        //console.log("[session]: Error retrieving session: ", err);
                        reject(err);
                    } else {
                        //console.log("[session]: Session: ", session);
                        resolve(session);
                    }
                })
            })
        }
    }

    async signOut() {
        if (this.user !== null) {
            await this.user.signOut();
        }
    }

    async isSessionValid() {
        //console.log("[isSessionValid]: Entering");
        try {
            const session: any = await this.session()
            //console.log("[isSessionValid]: Session: ", session);
    
            if (session !== undefined) {
                const isValid = session.isValid()
                //console.log("[isSessionValid]: Checking if session is valid ", isValid)
                return isValid
            } else {
                return false
            }
        } catch (e) {
            console.log('Error retrieving session')
            return false
        }
    }

    async authorize(username: string, password: string) {
        const authenticationData = {
            Username: username,
            Password: password,
        }
        const authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData)
        const userData = {
            Username: username,
            Pool: this.userPool
        }
        const cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData)
        return new Promise((resolve, reject) => {
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    resolve(true);
                },
                
                onFailure: function (err) {
                    console.log(err);
                    reject(false)
                }
            })
        })
    }
}

export default AWSSession
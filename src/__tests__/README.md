# How to test locally

To perform a local test of this library please perform the following steps:

* Copy file aws-exports-sample.ts into aws-exports.ts
* Edit aws-exports.ts and change required parameters using your own AWS settings
* run 

```sh
npm test
```

# aws-exports.ts

This file contains AWS settings. The following fields are mandatory to complete the test suite:

* aws_cognito_region
* CongnitoData
    * UserPoolId
    * ClientId
    * IndentityPoolId
* user
* password
* streamName


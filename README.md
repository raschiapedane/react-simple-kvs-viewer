# react-simple-kvs-viewer

A simple react component viewer for AWS KVS WebRTC.

## Description

react-simple-kvs-viewer is a simple react component able to playback video from AWS KVS WebRTC

**This library is still experimental and is therefore not yet suitable for production.**

## Install

Install using npm or yarn

```sh
npm i --save react-simple-kvs-viewer
```

```sh
yarn add react-simple-kvs-viewer
```

## props

* streamName - Name of the Signaling channel as defined in AWS
* region - Region where Signaling channel is hosted
* awsConfig - A valid AWS.config object with valid credentials (accessKeyId, secretAccessKey, sessionToken). These credentials need a valid IAM policy to interact with KVS

## Example

Import module

```JSX
import Cam from 'react-simple-kvs-viewer';
```

Use it into your render method as in this example.


```JSX
    render() {
        return (
            ...
            <Grid container spacing={2}>
                <Grid item xs={4}>
                    <Card className={'MyCard'}>    
                        <CardHeader className={'MyCardClassName'} title="My Cam"/>
                        <CardContent>
                            <Cam ref={this.Cam1} streamName='TBHouseStream' region='eu-west-1' awsConfig={AWS.config} />
                        </CardContent>
                    </Card>
                </Grid>
            ...
        )
    }





